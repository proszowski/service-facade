package eu.proszowski.surreptitiousknell.servicefacade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceFacadeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceFacadeApplication.class, args);
    }

}
